package tddEx;

import java.util.HashMap;

public class RomanToDecimal {

    private HashMap<Character, Integer> romanMap = new HashMap<>(){{
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
}};

    public int toDecimal(String roman) {
        int decimal = 0, i = 0;
        while(i + 1 < roman.length()) {
            int val1 = romanMap.get(roman.charAt(i));
            int val2 = romanMap.get(roman.charAt(i+1));
            if (val1 < val2) {
                decimal += val2 - val1;
                i++;
            } else {
                decimal += val1;
            }
            i++;
        }
        if(i < roman.length()) {
            decimal += romanMap.get(roman.charAt(i));
        }
        return decimal;
    }
}
