package tddEx;

public class NegativeNumberException extends Exception {
    public NegativeNumberException(String message) {
        super(message);
    }
}
