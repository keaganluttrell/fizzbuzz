package tddEx;

public class Calculator {
    public int add(String input) throws NegativeNumberException {
        if (input.equals("")) {
            return 0;
        }

        String[] inputArr = getDelimiter(input);
        String delimiter = inputArr[0];
        input = inputArr[1];

        String[] nums = input.split(",|\n|" + delimiter);
        int sum = 0;

        for (String num : nums) {
            if (Integer.valueOf(num) < 0) {
                throw new NegativeNumberException("Numbers can't be negative");
            }

            sum += Integer.valueOf(num);
        }

        return sum;
    }

    public String[] getDelimiter(String input) {
        String[] result = new String[2];
        int index = 0;
        result[0] = ",";

        if (input.charAt(0) == '/') {
            result[0] = input.substring(2, 3);
            index = 4;
        }
        result[1] = input.substring(index);

        return result;
    }
}
