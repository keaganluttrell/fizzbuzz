package tddEx;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RomanToDecimalTest {

    @Test
    public void romanToDecimalTest1() {
        RomanToDecimal r2d = new RomanToDecimal();

        int expected = 1;

        int actual = r2d.toDecimal("I");

        assertEquals(expected, actual, "'I' should be 1");

    }

    @Test
    public void romanToDecimalTest2() {
        RomanToDecimal r2d = new RomanToDecimal();

        int expected = 8;

        int actual = r2d.toDecimal("VIII");

        assertEquals(expected, actual, "'VIII' should be 8");

    }

    @Test
    public void romanToDecimalTest3() {
        RomanToDecimal r2d = new RomanToDecimal();

        int expected = 4;

        int actual = r2d.toDecimal("IV");

        assertEquals(expected, actual, "'IV' should be 4");

    }

    @Test
    public void romanToDecimalTest4() {
        RomanToDecimal r2d = new RomanToDecimal();

        int expected = 1944;

        int actual = r2d.toDecimal("MCMXLIV");

        assertEquals(expected, actual, "'MCMXLIV' should be 1944");

    }

}
