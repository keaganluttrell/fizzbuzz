package tddEx;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    @Test
    public void addMethodEmptyStringTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 0;
        int actual = calculator.add("");

        assertEquals(expected, actual, "Empty string should be 0");
    }

    @Test
    public void addMethodOneInputTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 7;
        int actual = calculator.add("7");

        assertEquals(expected, actual, "'7' should be 7");
    }

    @Test
    public void addMethodTwoInputsTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 3;
        int actual = calculator.add("1,2");

        assertEquals(expected, actual, "'1,2' should be 3");

    }

    @Test
    public void addMethodUnknownInputsCommaTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 10;
        int actual = calculator.add("1,2,3,4");

        assertEquals(expected, actual, "'1,2,3,4' should be 10");
    }

    @Test
    public void addMethodUnknownInputsNewLineTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 10;
        int actual = calculator.add("1\n2\n3\n4");

        assertEquals(expected, actual, "'1\n2\n3\n4' should be 10");
    }

    @Test
    public void addMethodUnknownInputsMixedDelimiterTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 10;
        int actual = calculator.add("1,2\n3\n4");

        assertEquals(expected, actual, "'1,2\n3\n4' should be 10");
    }

    @Test
    public void addMethodUnknownInputsCustomDelimiterTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        int expected = 10;
        int actual = calculator.add("//;\n1;2;3;4");

        assertEquals(expected, actual, "'1;2;3;4' should be 10");
    }

    @Test
    public void addMethodNegativeNumberTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        NegativeNumberException expected = assertThrows(NegativeNumberException.class, () -> {
            calculator.add("-10");
        });

        assertEquals("Numbers can't be negative", expected.getMessage());
    }

    @Test
    public void addMethodNegativeNumbersTest() throws NegativeNumberException {
        Calculator calculator = new Calculator();

        NegativeNumberException expected = assertThrows(NegativeNumberException.class, () -> {
            calculator.add("1,-10\n5");
        });

        assertEquals("Numbers can't be negative", expected.getMessage());
    }
}
