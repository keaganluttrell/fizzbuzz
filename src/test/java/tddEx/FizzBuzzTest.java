package tddEx;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FizzBuzzTest {
    @Test
    public void fizzBuzzMethodThirds() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "Fizz";
        String actual = app.fizzBuzz(6);
        assertEquals(expected, actual, "6 should be Fizz");
    }

    @Test
    public void fizzBuzzMethodFifths() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "Buzz";
        String actual = app.fizzBuzz(10);
        assertEquals(expected, actual, "10 should be Buzz");
    }

    @Test
    public void fizzBuzzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "FizzBuzz";
        String actual = app.fizzBuzz(15);
        assertEquals(expected, actual, "15 should be FizzBuzz");
    }

    @Test
    public void noFizzBuzzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "2";
        String actual = app.fizzBuzz(2);
        assertEquals(expected, actual, "2 should be '2'");
    }

    @Test
    public void negativeFizzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "Fizz";
        String actual = app.fizzBuzz(-3);
        assertEquals(expected, actual, "-3 should be Fizz");
    }

    @Test
    public void negativeBuzzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "Buzz";
        String actual = app.fizzBuzz(-5);
        assertEquals(expected, actual, "-5 should be Buzz");
    }

    @Test
    public void negativeFizzBuzzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "FizzBuzz";
        String actual = app.fizzBuzz(-15);
        assertEquals(expected, actual, "-15 should be FizzBuzz");
    }

    @Test
    public void negativeNonFizzBuzzMethod() {
        // this should be a method
        FizzBuzz app = new FizzBuzz();
        String expected = "-137";
        String actual = app.fizzBuzz(-137);
        assertEquals(expected, actual, "-137 should be '-137'");
    }
}
